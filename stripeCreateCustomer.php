<?php
  require __DIR__ . '/requestAuthenticator.php';
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  require __DIR__ . '/decodeJsonBody.php';
  require __DIR__ . '/stripeClient.php';

  $token = $POST_DATA["stripeToken"];
  $email = $POST_DATA['email'];

  $customer = \Stripe\Customer::create(array(
    "email" => $email,
    "source" => $token,
    "metadata" => array("CustomerCreatedBy" => "iPhone app"),
  ));

  echo json_encode($customer);
?>
