<?php
  require __DIR__ . '/client.php';
  header("Content-Type: application/json;charset=utf-8");
  $json = $woocommerce->get('customers', $parameters = []);
  echo json_encode($json);
 ?>
