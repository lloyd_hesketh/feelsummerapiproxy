<?php
  require __DIR__ . '/requestAuthenticator.php';
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  require __DIR__ . '/decodeJsonBody.php';
  require __DIR__ . '/stripeClient.php';

  $customerId = $POST_DATA['customerId'];
  $amount = $POST_DATA['amount'];
  $description = $POST_DATA['description'];

  $charge = \Stripe\Charge::create(array(
    "amount" => $amount,
    "currency" => "gbp",
    "customer" => $customerId,
    "description" => $description,
    "metadata" => array("CustomerChargeCreatedBy" => "iPhone app")
  ));

  echo json_encode($charge);
?>
