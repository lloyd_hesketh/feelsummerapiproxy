<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit877f16cf4575767de09934534d30aeca
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Stripe\\' => 7,
        ),
        'A' => 
        array (
            'Automattic\\WooCommerce\\' => 23,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Stripe\\' => 
        array (
            0 => __DIR__ . '/..' . '/stripe/stripe-php/lib',
        ),
        'Automattic\\WooCommerce\\' => 
        array (
            0 => __DIR__ . '/..' . '/automattic/woocommerce/src/WooCommerce',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit877f16cf4575767de09934534d30aeca::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit877f16cf4575767de09934534d30aeca::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
