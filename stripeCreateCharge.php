<?php
  require __DIR__ . '/requestAuthenticator.php';
  // ini_set('display_errors', 1);
  // ini_set('display_startup_errors', 1);
  // error_reporting(E_ALL);
  require __DIR__ . '/decodeJsonBody.php';
  require __DIR__ . '/stripeClient.php';

  $token = $POST_DATA["stripeToken"];
  $amount = $POST_DATA['amount'];
  $description = $POST_DATA['description'];

  // Charge the user's card:
  $charge = \Stripe\Charge::create(array(
    "amount" => $amount,
    "currency" => "gbp",
    "description" => $description,
    "metadata" => array("ChargeCreatedBy" => "iPhone app"),
    "source" => $token,
  ));

  echo json_encode($charge);

?>
